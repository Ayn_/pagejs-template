const path = require('path')
    , pagejsWebpackPlugin = require('pagejs-webpack-plugin')

module.exports = {
    entry: './index.page.js',
    resolve: {
        extensions: ['.page.js', '.js']
    },
    module: {
        rules: [
            //
        ]
    },
    plugins: [
        new pagejsWebpackPlugin()
    ],
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    }
}